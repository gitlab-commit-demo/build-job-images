# Gitlab Commit Demo

## Job images

This repo is part of my Demo at Gitlab Commit. The repo can be used to build pipeline job images.  
A guide on how to install an AKS cluster to host your Gitlab Runner can be found [here](https://gitlab.com/gitlab-commit-demo).

## TL;DR

1. Clone this repo
2. Execute the pipeline

> Make sure that you mark your runner with "kubernetes" if you have not created it with the linked repo!
